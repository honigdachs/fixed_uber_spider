# -*- coding: utf-8 -*-
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class UberMailer():
    efrom = "support@hyrecar.com"
    eto = "your@email.com"
    esubject = "[HyreCar]%s"
    ehtml = """\
        <html>
            <head></head>
            <body>
                <p>Hi, %s<br><br>
                    %s
                    <br><br><br>
                    Have any questions? Just email us at support@hyrecar.com
                </p>
            </body>
        </html>
        """
    ehtml2 = """\
        <html>
            <head></head>
            <body>
                %s
            </body>
        </html>
        """
    ename = None
    elogintext = "We could not complete your Uber integration because login failed. Please update your Uber credentials and try again."
    eupdatetext = "We could not continue updating your data because login failed. Please update your Uber credentials and try again."
    eaccountactive = "Your Uber account is now active! Login to the following link using your Uber account:<br /><br /> http://uber.hyrecar.com/"
    eexception = "An error occurred during a Uber spider operation: %s, <br /><br />Email address: %s, User ID: %s"
    etext = ""

    def init(self, mail_parts=None):
        self.eto = mail_parts['to']
        self.esubject = self.esubject % mail_parts['subject']
        
        if 'name' not in mail_parts.keys():
            mail_parts['name'] = ""

        if 'user_id' not in mail_parts.keys():
            mail_parts['user_id'] = ""

        if 'email' not in mail_parts.keys():
            mail_parts['email'] = ""

        self.ename = mail_parts['name']

        if "login" in mail_parts['tpl']:
            self.etext = self.elogintext
        elif "active" in mail_parts['tpl']:
            self.etext = self.eaccountactive
        elif "exception" in mail_parts['tpl']:
            self.etext = self.eexception % (mail_parts['exception'], mail_parts['email'], mail_parts['user_id'])
            self.ehtml = self.ehtml2
        else:
            self.etext = self.eupdatetext

    def send(self, body=None):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = self.esubject
        msg['From'] = self.efrom
        msg['To'] = self.eto

        part1 = MIMEText(self.etext, 'plain')
        part2 = MIMEText(self.ehtml % (self.ename, self.etext), 'html')

        msg.attach(part1)
        msg.attach(part2)

        s = smtplib.SMTP('localhost')
        s.sendmail(self.efrom, self.eto, msg.as_string())
        s.quit()

        return None