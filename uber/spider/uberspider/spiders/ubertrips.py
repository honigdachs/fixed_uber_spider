# -*- coding: utf-8 -*-
import scrapy
import datetime
import mysql.connector
from scrapy.http import FormRequest, Request
from scrapy.selector import Selector
from uberspider.items import UberItem
from uberspider.libraries import UberMailer
from uberspider.hyrecar import HyrecarDB
from scrapy.utils.response import open_in_browser


class UbertripsSpider(scrapy.Spider):
	name = "ubertrips"
	allowed_domains = ["uber.com"]
	start_urls = (
		'http://login.uber.com/login',
	)

	def __init__(self, email_address, password, user_id=None, **kwargs):
		db = HyrecarDB()
		self.conn = db.conn
		self.cursor = db.cursor
		self.uber_user=dict(email=email_address,password=password)
		self.uber_user_id=user_id

	def parse(self, response):
		return [FormRequest.from_response(response,
			formdata=self.uber_user,
			callback=self.after_login, dont_filter=True)]

	def after_login(self, response):
		# check login succeed before going on

		if "Invalid email or password" in response.body:
			print "LOGIN FAILED"
			self.notify(dict(to=self.uber_user['email'], subject="Action Required", tpl="update"))
			return None
		# We've successfully authenticated, let's have some fun!
		else:
			print 'SUCCESSFULLY LOGGED IN'
			return Request(url="https://partners.uber.com/home/",
				callback=self.parse_info, dont_filter=True)

	def notify(self, mail_parts=None):
		#get name
		self.cursor.execute("SELECT name FROM uber_users WHERE email_address='%s'" % mail_parts['to'])
		me = self.cursor.fetchone()
		if me[0] is not None:
			mail_parts['name'] = me[0]
		#init mailer
		#mailer = UberMailer()
		#mailer.init(mail_parts)
		#mailer.send()

	def throw(self, mail_parts=None):
		#init mailer
		#print 'THROW'
		#print mail_parts
		mailer = UberMailer()
		mailer.init(mail_parts)
		mailer.send()

	def parse_info(self, response):
		try:
			print 'parsing driver trips page'
			xpath = '/html/body/div[1]/div/div/table[2]/tbody/tr'
			xpath_next = '/html/body/div[1]/div/div/table/tbody/tr'
			xpath_use = xpath
			u_trips = Selector(response=response).xpath(xpath).extract()

			if len(u_trips) == 0:
				u_trips = Selector(response=response).xpath(xpath_next).extract()
				xpath_use = xpath_next

			u_now = datetime.datetime.now()
			u_created_at = u_now.strftime('%Y-%m-%d %H:%M:%S')
			for trip in range(1, len(u_trips)):
				item = UberItem()

				u_id = Selector(response=response).xpath('%s[%s]/td[1]/a/@href' % (xpath_use, trip)).extract()
				u_date = Selector(response=response).xpath('%s[%s]/td[1]/a/text()' % (xpath_use, trip)).extract()

				try:
					s_date = datetime.datetime.strptime(u_date[0][:-4], '%B %d, %Y %I:%M%p')
				except Exception, e:
					s_date = datetime.datetime.strptime(u_date[0][:-4], '%B %d, %Y %I%p')
				
				u_duration = Selector(response=response).xpath('%s[%s]/td[2]/text()' % (xpath_use, trip)).extract()
				u_mileage = Selector(response=response).xpath('%s[%s]/td[3]/text()' % (xpath_use, trip)).extract()
				u_fare = Selector(response=response).xpath('%s[%s]/td[4]/text()' % (xpath_use, trip)).extract()
				u_status = Selector(response=response).xpath('%s[%s]/td[5]/text()' % (xpath_use, trip)).extract()
				
				item['user_id'] = self.uber_user_id
				item['trip_id'] = u_id[0].replace("/trips/", "")
				item['trip_date'] = s_date.strftime('%Y-%m-%d %H:%M')
				item['duration'] = u_duration[0]
				item['mileage'] = u_mileage[0]
				item['fare'] = u_fare[0].replace("$", "")
				item['status'] = u_status[0]
				item['created_at'] = u_created_at

				# items.append(item)
				yield item

			# If there is a next page, extract href, build request
			# and send it to server
			next_page = response.xpath('//a[contains(text(), "Next")]/@href')
			if next_page:
				next_page_href = next_page.extract()[0]
				next_page_url = response.urljoin(next_page_href)
				request = scrapy.Request(next_page_url, callback=self.parse_info)
				yield request
		except Exception, e:
			self.throw(dict(to='j0hnw0rk3r@gmail.com', subject="Failed ubertrips/parse_info", tpl="exception", email=self.uber_user['email'], exception=str(e)))
