# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class UberItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    user_id = scrapy.Field()
    trip_id = scrapy.Field()
    trip_date = scrapy.Field(serializer=str)
    created_at = scrapy.Field(serializer=str)
    duration = scrapy.Field()
    mileage = scrapy.Field()
    fare = scrapy.Field()
    status = scrapy.Field()
