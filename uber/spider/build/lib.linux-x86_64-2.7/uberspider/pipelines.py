# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import mysql.connector
import datetime
from uberspider.hyrecar import HyrecarDB

class UberPipeline(object):
	def __init__(self):
		db = HyrecarDB()
		self.conn = db.conn
		self.cursor = db.cursor

	def process_item(self, item, spider):
		self.cursor.execute("SELECT * FROM uber_trips WHERE trip_id = '%s' and user_id = %s" % (item['trip_id'].encode('utf-8'), item['user_id']))
		old_trip = self.cursor.fetchone()
		if old_trip is None:
			self.cursor.execute("""INSERT INTO uber_trips(user_id, trip_id, trip_date, duration, mileage, fare, status, created_at)
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""", (item['user_id'], item['trip_id'].encode('utf-8')
					, item['trip_date'].encode('utf-8'), item['duration'].encode('utf-8')
					, item['mileage'].encode('utf-8'), item['fare'].encode('utf-8')
					, item['status'].encode('utf-8'), item['created_at'].encode('utf-8')))
			self.conn.commit()
			return item
		else:
			print "Trip exists: trip # %s" % item['trip_id'].encode('utf-8')