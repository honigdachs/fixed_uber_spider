#-*- coding: utf-8 -*-
import scrapy
import datetime
import mysql.connector
import string
from lxml import etree
from scrapy.http import FormRequest, Request
from scrapy.selector import Selector
from uberspider.items import UberItem
from mailer import Mailer
from mailer import Message
from uberspider.libraries import UberMailer
from uberspider.hyrecar import HyrecarDB

class UberloginSpider(scrapy.Spider):
	name = "uberlogin"
	allowed_domains = ["uber.com"]
	start_urls = (
		'http://login.uber.com/login',
	)

	def error(results):
		print 'error'
		return

	def __init__(self, email_address=None, password=None, **kwargs):
		db = HyrecarDB()
		self.conn = db.conn
		self.cursor = db.cursor
		self.uber_user=dict(email=email_address,password=password)
		
	def parse(self, response):
		return [FormRequest.from_response(response,
			formdata=self.uber_user,
			callback=self.after_login, dont_filter=True)]

	def after_login(self, response):
		# check login succeed before going on
		if "Invalid email or password" in response.body:
			print "LOGIN FAILED"
			self.notify(dict(to=self.uber_user['email'], subject="Action Required", tpl="login", name="Anonymous"))
			self.archive_user(self.uber_user['email'])
			return None
		# We've successfully authenticated, let's have some fun!
		else:
			print 'SUCCESSFULLY LOGGED IN'
			return Request(url="https://partners.uber.com/home/",
				callback=self.parse_info, dont_filter=True)

	def notify(self, mail_parts=None):
		#get name
		# self.cursor.execute("SELECT name FROM uber_users WHERE email_address='%s'" % mail_parts['to'])
		# me = self.cursor.fetchone()
		if mail_parts['name'] is None:
			mail_parts['name'] = ""
		#init mailer
		mailer = UberMailer()
		mailer.init(mail_parts)
		mailer.send()

	def archive_user(self, email):
		archive_user = ("UPDATE uber_users "
			"SET status = %s"
			"where email_address= %s")
		data_user = ("Archive", email)

		self.cursor.execute(archive_user, data_user)
		self.conn.commit()

	def parse_info(self, response):
		#extract info ang create user
		myname = Selector(response=response).xpath('/html/body/div/nav[2]/div/ul/li[2]/div/ul/li[1]/a/text()').extract()
		myrating = Selector(response=response).xpath('/html/body/div/nav[2]/div/ul').extract()

		name = self.clean_str(myname[0])
		rating = self.get_rating(myrating[0])

		update_user = ("UPDATE uber_users "
			"SET name = %s, rating = %s, status = %s"
			"where email_address= %s")
		data_user = (name, rating, "Active", self.uber_user['email'])

		self.cursor.execute(update_user, data_user)
		self.conn.commit()

		#notify user
		self.notify(dict(to=self.uber_user['email'], subject="Uber Account Status", tpl="active", name=name))

		return None

	def get_rating(self, htm):
		tree = etree.fromstring(htm); 
		_rating = etree.tostring(tree.xpath("//li")[0], pretty_print=True)
		rating = self.find_between(_rating, '<span class="icon icon_star alpha push-half--right"/>', '</li>')
		return self.clean_str(rating)

	def clean_str(self, str):
		filtered = filter(lambda x: x in string.printable, str.strip()).strip()
		return filtered

	def find_between(self, s, first, last ):
		try:
			start = s.index( first ) + len( first )
			end = s.index( last, start )
			return s[start:end]
		except ValueError:
			return ""
