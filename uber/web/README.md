### Deploying Uber Scraper (Web App and Python Spider) into test & production servers
`Note`: If the composer phar is not installed you will have to install it and to intsall composer you would need ssl enabled on PHP. `PHP v5.4` and above is needed.

```command
##Run the following command once
cd /home/hyrecar.com/public_html/scrapers/uber/web
composer install
```

#### Updating Web App (Laravel 5.1) and Python Scraper (Scrapy & Scrapyd)
```command
$ cd hyrecar/scrapers/uber/web
$ cp -rf app/ /home/hyrecar.com/public_html/scrapers/uber/web/
$ cp -rf public/ /home/hyrecar.com/public_html/scrapers/uber/web/
$ cp -rf resources/ /home/hyrecar.com/public_html/scrapers/uber/web/
```

```command
$ cd hyrecar/scrapers/uber/spider
$ cp -rf * /home/hyrecar.com/public_html/scrapers/uber/spider/
$ cd /home/hyrecar.com/public_html/scrapers/uber/spider/
$ scrapyd-deploy #this should deploy the uber spider into scrapyd
```