@extends('layout')
@section('content')
<div class="container" style="margin-top:40px">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong> Sign in to continue</strong>
                </div>
                <div class="panel-body">
                    @if(Session::has('flash-msg'))
                        <h4>Login Status</h4>
                            <p>{{ Session::get('flash-msg') }}</p>
                      </div>
                    @endif
                    @if($errors->any())
                      <div class="callout callout-info">
                        <h4>Error</h4>
                        <p>{{ implode('', $errors->all(':message')) }}</p>
                      </div>
                    @endif
                    {!! Form::open(array('url' => '/login2')) !!}
                    <!-- <form role="form" action="http://bootsnipp.com/iframe/947G4#" method="POST"> -->
                        <fieldset>
                            <div class="row">
                                <div class="center-block" style="text-align: center">
                                    <h4>&nbsp;</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                            <input class="form-control" placeholder="Email" name="email" type="text" autofocus="" autocomplete="off" value="{{ $email }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-lock"></i>
                                            </span>
                                            <input class="form-control" placeholder="Password" name="password" type="password" value="" autocomplete="off" value="fortesting@0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-lg btn-primary btn-block" value="Sign in">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="center-block" style="text-align: center">
                                    <!-- <img class="profile-img" src="photo.jpg" alt=""> -->
                                    <h4>&nbsp;</h4>
                                </div>
                            </div>
                        </fieldset>
                    <!-- </form> -->
                    {!! Form::close() !!}
                </div>
                <div class="panel-footer ">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>
@stop