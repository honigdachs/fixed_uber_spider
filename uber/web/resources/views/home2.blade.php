@extends('layout')
@section('content')
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="{{URL::asset('js/chart.js')}}"></script>
<div class="container">
    <div class="row">
        <div class="col-md-12 chart">
            <div class="panel panel-default">
                <div class="panel-heading" style="padding: 15px">
                    <strong>My Uber Statistics</strong>
                    <div class="pull-right">
                        <span class="glyphicon glyphicon-star"></span> {{$rating}} &nbsp;&nbsp;
                        <span class="glyphicon glyphicon-user"></span> {{$name}}
                    </div>
                </div>
                <div class="panel-body">
                  <div class="col-md-12 chart-opt">
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="">Review</label>
                            <select class="form-control" id="filterReview">
                                <option>Select</option>
                                <option value="weekly">Weekly</option>
                                <option value="monthly" selected="">Monthly</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="">Year</label>
                            <select class="form-control" id="year">
                                @foreach ($years as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3 hidden toggle-opt" id="toggle-weekly">
                            <label for="">Select Week</label>
                            <select class="form-control toggle-filter" id="weekly">
                            </select>
                        </div>
                        <div class="form-group col-md-3 hidden toggle-opt" id="toggle-monthly">
                            <label for="">Select Month</label>
                            <select class="form-control toggle-filter" id="monthly">
                            </select>
                        </div>
                    </div>
                  </div>
                    <div class="col-md-12" id="chart_div"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .container {
        width: 100% !important;
        padding: 0px 0px !important;
    }
</style>
<script type="text/javascript">
    window.addEventListener("message", receiveMessage, false);

    function sendMessage() {
        window.parent.postMessage({action: 'setHeight', height: $(document).height()}, '*');
    }
    function receiveMessage(event) {
        event.source.postMessage({action: 'setHeight', height: $(document).height()}, '*');
    }
    var autoLoad = true;
</script>
@stop