@extends('layout')
@section('content')
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="{{URL::asset('js/chart.js')}}"></script>
<div class="container" style="margin-top:40px">
    <div class="row">
        <div class="col-sm-12 col-md-12 chart">
            <div class="panel panel-default">
                <div class="panel-heading" style="padding: 15px">
                    <span class="glyphicon glyphicon-home">&nbsp;</span><strong>Welcome to Uber Partner</strong>
                    <div class="pull-right"><span class="glyphicon glyphicon-star"></span> {{$rating}} &nbsp;&nbsp;
                    <span class="glyphicon glyphicon-user"></span> {{$name}} &nbsp; <a href="{{url('logout')}}">Logout</a></div>
                </div>
                <div class="panel-body">
                  <div class="col-xs-10 chart-opt">
                    <div class="form-group col-xs-2">
                        <label for="">Review</label>
                        <select class="form-control" id="filterReview">
                            <option>Select</option>
                            <option value="weekly">Weekly</option>
                            <option value="monthly">Monthly</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="">Year</label>
                        <select class="form-control" id="year">
                            @foreach ($years as $year)
                                <option value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-xs-3 hidden toggle-opt" id="toggle-weekly">
                        <label for="">Select Week</label>
                        <select class="form-control toggle-filter" id="weekly">
                        </select>
                    </div>
                    <div class="form-group col-xs-3 hidden toggle-opt" id="toggle-monthly">
                        <label for="">Select Month</label>
                        <select class="form-control toggle-filter" id="monthly">
                        </select>
                    </div>
                  </div>
                    <div class="col-md-12" id="chart_div"></div>
                </div>
                <!-- <div class="panel-footer ">
                    &nbsp;
                </div> -->
            </div>
        </div>
    </div>
</div>
@stop