<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Uber Partner | {{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
    <script src="{{URL::asset('js/jquery-1.10.2.min.js')}}"></script>
    <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
    
	<link rel="stylesheet" type="text/css" href="{{URL::asset('css/chart.css')}}">
	<!--Load the AJAX API-->
	<script type="text/javascript">
	    var baseUrl = "{{url()}}/";
	</script>
</head>
<body>
    @yield('content')
</body>
</html>