google.load('visualization', '1.0', {
    'packages': ['corechart']
});
var Chart = function() {
    return {
        init: function(argument) {},
        filter: function() {
            var filter = $('#filterReview').val(),
                year = $('#year').val();
            if (filter.trim().length === 0 || year.trim().length === 0) return;
            $.ajax({
                url: baseUrl + '/dates/' + filter,
                method: 'GET',
                dataType: 'json',
                data: {
                    year: year
                },
                success: function(response) {
                    if (response.length === 0) return;
                    $('.toggle-opt').addClass('hidden');
                    $('#toggle-' + filter + ' select').html('');
                    var options = '<option>Select</option>';
                    response.data.forEach(function(opt) {
                        if (filter === 'weekly') {
                            options += '<option value="' + opt.month + ',' + opt.week + '">' + opt.month + ' - Week ' + opt.week + '</option>';
                        } else if (filter === 'monthly') {
                            options += '<option value="' + opt.month + '">' + opt.month + '</option>';
                        }
                    });
                    $('#toggle-' + filter).removeClass('hidden').find('select').append(options);

                    if(autoLoad === true) {
                        Chart.update();
                        var ffilter = $('#filterReview').val();
                            ffilter_val = $('#' + filter + ' option:nth-child(2)')[0].value;
                        $('#' + filter).val(ffilter_val);
                        $('#' + filter).change();
                    }
                },
                error: function(argument) {
                    // body...
                }
            });
        },
        update: function() {
            var filter = $('#filterReview').val(),
                year = $('#year').val();
            filter_val = $('#' + filter).val();
            if (filter.trim().length === 0 || year.trim().length === 0 || filter_val.trim().length === 0 || filter_val.trim() === 'Select') return;
            $.ajax({
                url: baseUrl + '/data/' + filter,
                method: 'GET',
                dataType: 'json',
                data: {
                    year: year,
                    filter_val: filter_val
                },
                success: function(response) {
                    if (response.length === 0) return;
                    Chart['draw_' + filter](response);
                },
                error: function(argument) {
                    // body...
                }
            });
        },
        draw_weekly: function(response) {
            // $('#chart_div').html();
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Weekday');
            data.addColumn('number', 'Mileage');

            var dates = [];
            response.stats.forEach(function(date) {
                dates.push([date.weekday, parseFloat(date.mileage)]);
            });
            data.addRows(dates);
            // Set chart options
            var options = {
                'title': 'Driving Statistics - ' + $("#filterReview").val().toUpperCase(),
                'width': "auto",
                'height': 500,
                'x': -100,
                'y': -100,
                'padding': 0,
                'margin': 0,
                is3D: true
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);

            setTimeout(sendMessage, 100);
        },
        draw_monthly: function(response) {
            // $('#chart_div').html();
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Weeks');
            data.addColumn('number', 'Mileage');

            var dates = [];
            response.stats.forEach(function(date) {
                dates.push(['Week ' + date.week, parseInt(date.mileage)]);
            });
            data.addRows(dates);
            // Set chart options
            var options = {
                'title': 'Driving Statistics - ' + $('#' + $("#filterReview").val()).val().toUpperCase(),
                'width': "auto",
                'height': 500,
                'x': -100,
                't': -100,
                'padding': 0,
                'margin': 0,
                is3D: true
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);

            setTimeout(sendMessage, 100);
        }
    };
}();
$(document).ready(function(argument) {
    Chart.init();
    $('#filterReview').change(function(e) {
        Chart.filter();
    });
    $('.toggle-filter').change(function(e) {
        Chart.update();
    });

    if(autoLoad === true) {
        $('#filterReview').change();
        // Chart.update();
    }
});