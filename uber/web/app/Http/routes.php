<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/login', function () {
    return view('login', ['title' => 'Sign In', 'email' => Input::get('ref')]);
});

Route::get('/login2', function () {
    return view('login2', ['title' => 'Sign In', 'email' => Input::get('ref')]);
});

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('/home/');
    } 
    else return redirect('/login');
});

Route::get('/my_trip', ['middelware' => 'user', 'uses' => 'TripController@getTrip']);
Route::get('/home', ['middelware' => 'user', 'uses' => 'HomeController@getHome']);
Route::get('/home2', 'HomeController@getHome2');
Route::get('/dates/weekly', ['middelware' => 'user', 'uses' => 'TripController@getWeekly']);
Route::get('/dates/monthly', ['middelware' => 'user', 'uses' => 'TripController@getMonthly']);

Route::get('/data/weekly', ['middelware' => 'user', 'uses' => 'TripController@getDataWeekly']);
Route::get('/data/monthly', ['middelware' => 'user', 'uses' => 'TripController@getDataMonthly']);

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/login');
});

Route::post('/login', 'Auth\AuthController@postLogin');
Route::post('/login2', 'Auth\AuthController@postLogin2');
Route::post('/link', 'UberController@postLink');

//route to call by cron
Route::get('/data/batch', 'TripController@getDataBatch');
