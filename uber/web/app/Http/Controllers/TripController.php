<?php
namespace App\Http\Controllers;

// use App\User;
use Validator;
use Illuminate\Routing\Controller;
use App\Models\Uber;

class TripController extends Controller
{
    public function getUber() {
        return Uber::getUber();
    }
    public function getWeekly() {
        return Uber::getWeekly();
    }
    public function getMonthly() {
        return Uber::getMonthly();
    }
    public function getDataWeekly() {
        return Uber::getDataWeekly();
    }
    public function getDataMonthly() {
        return Uber::getDataMonthly();
    }
    public function getDataBatch() {
        return Uber::getDataBatch();
    }
}
