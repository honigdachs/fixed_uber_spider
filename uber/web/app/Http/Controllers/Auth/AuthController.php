<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Illuminate\Routing\Controller;
use App\Models\Users;
use App\Http\Requests\LoginPostRequest;

class AuthController extends Controller
{
    public function postLogin(LoginPostRequest $request)
    {
        return Users::postLogin();
    }
    public function postLogin2(LoginPostRequest $request)
    {
        return Users::postLogin2();
    }
    public function postLink(LoginPostRequest $request)
    {
        return Users::postLink();
    }
}
