<?php
namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Routing\Controller;
use App\Models\Uber;
use Auth;

class HomeController extends Controller
{
    public function getHome() {
        $years = Uber::getYears();
        $user = Auth::user();
        return view('home', [
            'title' => 'My Performance',
            'years' => $years,
            'name' => $user['name'],
            'rating' => $user['rating']
        ]);
    }
    public function getHome2() {
    	$years = Uber::getYears();
        $user_id = \Input::get('user');

        if(!Auth::check()) {
            if(!$user_id || empty($user_id))
                return redirect('/login2?user_id=' . $user_id)->withErrors('Please login with your Uber account in order to view this page!');

            $user = User::find($user_id);
            if(!$user)
                return redirect('/login2?user_id=' . $user_id)->withErrors('Please login with your Uber account in order to view this page!');

            \Auth::login($user);
        }

        $user = \Auth::user();

        \App\Models\Uber::getTrips($user->email_address, \App\Models\Secrets::getSecret($user->id), $user->id);
        return view('home2', [
        	'title' => 'My Performance',
        	'years' => $years,
        	'name' => $user['name'],
        	'rating' => $user['rating']
        ]);
    }
}
