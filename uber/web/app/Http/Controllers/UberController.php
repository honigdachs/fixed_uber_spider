<?php
namespace App\Http\Controllers;

// use App\User;
use Validator;
use Illuminate\Routing\Controller;
use App\Models\Uber;

class UberController extends Controller
{
    public function postLink() {
        try {
            $email = \Input::get('email');
            $password = \Input::get('password');
            $int_user_id = \Input::get('int_user_id');
            
            $filter = [
                'email_address' => $email
            ];
            $user = \App\Models\Users::where($filter)->first();
            if (isset($user['id'])) {
                if($user['status'] == 'Archive') {
                    $user->status = 'Pending';
                    $user->password = bcrypt($password);
                    $user->save();

                    \App\Models\Secrets::updateSecret($user['id'], $password);
                    \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user->id));

                    return \Response::json([
                        'msg' => 'Your Uber account is being validated. '.
                            'You will be notified via email when your account has been validated.'
                    ], 500);
                }
                elseif($user['status'] == 'Pending') {
                    \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user->id));
                    return \Response::json([
                        'msg' => 'Your uber account is being validated! Please try again later.'
                    ], 500);
                }
                else {
                    return \Response::json(null, 200);
                }
            }
            else {
                $user = new \App\User();
                $user->email_address = $email;
                $user->password = bcrypt($password);
                $user->int_user_id = $int_user_id;
                $user->status = 'Pending';

                $user->save();

                // Create secret password
                \App\Models\Secrets::createSecret($user['id'], $password);
                \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user->id));

                return \Response::json([
                    'msg' => 'Your Uber account is being validated. '.
                        'You will be notified via email when you can start using your account.'
                ], 200);
            }
        } catch (\Exception $e) {
            return \Response::json($e->getMessage(), 500);
        }
    }
}
