<?php
namespace App\Http\Middleware;

use Closure;
use Auth;
use App;
use Request;
use Response;

class UserMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next) {
        if (!Auth::check()) {
            if (Request::ajax()) {
                return Response::json(['success' => FALSE]);
            } 
            else {
                return redirect('/login')->withErrors('You are not logged in');
            }
        } 
        else {
            return $next($request);
        }
    }
}
