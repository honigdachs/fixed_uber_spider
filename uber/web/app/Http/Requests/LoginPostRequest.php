<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use App\Models\Users;
use Input;
use Session;
use Redirect;

class LoginPostRequest extends Request
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $data = Input::all();
        
        $rules = ['email' => 'required|email', 'password' => 'required'];
        return $rules;
    }
}
