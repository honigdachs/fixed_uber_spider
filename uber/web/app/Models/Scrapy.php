<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scrapy extends Model
{
    public static function schedule($args = []) {
    	// where are we posting to?
		$url = env('SCRAPYD_HOST', 'http://localhost:6800/schedule.json');
		// $url = 'http://localhost:6800/schedule.json';

		// what post fields?
		$fields = array(
		   'project' => 'uberspider'
		);

		$fields = array_merge($fields, $args);

		// build the urlencoded data
		$postvars = http_build_query($fields);

		// open connection
		$ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);

		// execute post
		curl_exec($ch);

		// close connection
		curl_close($ch);
    }
}
