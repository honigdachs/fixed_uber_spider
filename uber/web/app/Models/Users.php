<?php
namespace App\Models;
use DB;
use Input;
use Response;
use Eloquent;
use File;
use Redirect;
use Auth;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use App\Models\Uber;
use App\Models\Secrets;
use App\Models\Scrapy;
use Session;

class Users extends Eloquent
{
    protected $table = 'users';
    protected $guarded = array();
    
    public static function postLogin() {
        $email = Input::get('email');
        $password = Input::get('password');
        
        $filter = [
            'email_address' => $email
        ];
        $user = Users::where($filter)->first();
        
        if (isset($user['id'])) {
            if($user['status'] == 'Pending') {
                \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user['id']));
                return redirect('/login')->withErrors('Your uber account is currenlty being validated in our system! Please try again later.');
            }
            elseif(Auth::attempt([
                'email_address' => $email, 
                'password' => $password
            ])) {
                // Users::getEncrypted($email);
                return redirect('/home');
            }
            else {
                return redirect('/login')->withErrors('Invalid username/password!');
            }
        }
        else {
            $user = new \App\User();
            $user->email_address = $email;
            $user->password = bcrypt($password);
            $user->status = 'Pending';
            $user->save();

            // Create secret password
            Secrets::createSecret($user['id'], $password);
            \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user['id']));
            
            Session::flash('flash-msg', 'Your uber account is being validated. '.
                    'You will be notified via email when you can start using your account.');

            return redirect('/login');
        }
    }

    public static function postLogin2() {
        $email = Input::get('email');
        $password = Input::get('password');
        $session = \Session::get('pre-login');
        
        $filter = [
            'email_address' => $email
        ];
        $user = Users::where($filter)->first();
        if (isset($user['id'])) {
            if($user['status'] == 'Archive') {
                $user->status = 'Pending';
                $user->password = bcrypt($password);
                $user->save();

                Secrets::updateSecret($user['id'], $password);
                \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user->id));

                Session::flash('flash-msg', 'In the process, your uber account is being validated. '.
                        'You will be notified via email when you can start using your account.');

                return redirect('/login2');
            }
            elseif($user['status'] == 'Pending') {

                if($session && isset($session['user_id'])) {
                    $user->int_user_id = $session['user_id'];
                    $user->save();
                }
                
                \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user->id));
                return redirect('/login2')->withErrors('Your uber account is being validated! Please try again later.');
            }
            elseif(Auth::attempt([
                'email_address' => $email, 
                'password' => $password
            ])) {
                if($session && isset($session['user_id'])) {
                    $user->int_user_id = $session['user_id'];
                    $user->save();
                }
                self::getEncrypted($email);

                return redirect('/home2?user_id=' . $session['user_id']);
            }
            else {
                return redirect('/login2')->withErrors('Invalid username/password!');
            }
        }
        else {
            $user = new \App\User();
            $user->email_address = $email;
            $user->password = bcrypt($password);
            $user->status = 'Pending';

            if($session && isset($session['user_id'])) {
                $user->int_user_id = $session['user_id'];
            }

            $user->save();

            // Create secret password
            Secrets::createSecret($user['id'], $password);
            \App\Models\Uber::getLogin($email, \App\Models\Secrets::getSecret($user->id));

            Session::flash('flash-msg', 'In the process, your uber account is being validated. '.
                    'You will be notified via email when you can start using your account.');

            return redirect('/login2');
        }
    }

    public static function getEncrypted ($email = null) {
        if(is_null($email)) {
            $user = Auth::user();
        }
        else {
            $user = Users::where(['email_address' => $email])->first();
        }
        $password_raw = Secrets::getSecret($user['id']);
        Uber::getTrips($email, $password_raw, $user['id']);
    }
}
