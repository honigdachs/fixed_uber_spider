<?php
namespace App\Models;
use Eloquent;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class Secrets extends Eloquent
{
    protected $table = 'secrets';
    protected $guarded = array();
    
    public static function encrypt($secret) {
        return Crypt::encrypt($secret);
    }
    
    public static function decrypt($encrypted) {
        try {
            return Crypt::decrypt($encrypted);
        }
        catch(DecryptException $e) {
            exit($e);
        }
    }
    
    public static function createSecret($id, $raw) {
        $secret = new Secrets();
        $secret->user_id = $id;
        $secret->secret = Secrets::encrypt($raw);
        $secret->save();
    }
    
    public static function updateSecret($id, $raw) {
        $secret = Secrets::where(['user_id' => $id])->first();
        if($secret) {
            $secret->secret = Secrets::encrypt($raw);
            $secret->save();
        }
    }
    
    public static function getSecret($id) {
        $secret = Secrets::where(['user_id' => $id])->first();
        return Secrets::decrypt($secret['secret']);
    }
}
