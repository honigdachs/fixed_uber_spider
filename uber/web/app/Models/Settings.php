<?php
namespace App\Models;
use DB;
use Input;
use Response;
use Eloquent;
use Redirect;

class Settings extends Eloquent
{
    protected $table = 'settings';
    protected $guarded = array();
    
    public static function get_root() {
        $base = base_path();
        chdir($base . '/../spider');
        return getcwd();
    }

    public static function get_by($setting='')
    {
        if(empty($setting)) return false;

        $settings = Settings::where('setting', $setting)->first();
        if(isset($settings['id'])) {
            return $settings['value'];
        }
        else {
            return false;
        }
    }
}