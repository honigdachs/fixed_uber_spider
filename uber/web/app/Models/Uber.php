<?php
namespace App\Models;
use DB;
use Input;
use Response;
use Eloquent;
use Redirect;
use Auth;
use App\Models\Users;
use App\Models\Settings;
use App\Models\Scrapy;

class Uber extends Eloquent
{
    protected $table = 'trips';
    protected $guarded = array();
    protected static $scraper_root = '';

    public static function getTrip() {
        $user = Auth::user();
        $trip_dates = Uber::where('user_id', $user['id'])
                        ->where('status', '=', 'Complete')
                        ->select(DB::raw('date_format(trip_date, "%M %Y") as trip_date'))
                        ->groupBy(DB::raw('date_format(trip_date, "%M %Y")'))
                        ->orderBy('trip_date', 'asc')
                        ->get();

        $dates = [];
        foreach ($trip_dates as $key => $trip_date) {
            $trip_count = Uber::where(DB::raw('date_format(trip_date, "%M %Y")'), $trip_date['trip_date'])
                                    ->where('status', '=', 'Complete')
                                    ->count();
            $dates[] = ['period' => $trip_date['trip_date'], 'count' => $trip_count];
        }

        return Response::json(['success' => true, 'dates' => $dates]);
    }

    public static function getYears() {
        $user = Auth::user();
        $trip_dates = Uber::where('user_id', $user['id'])
                        ->where('status', '=', 'Complete')
                        ->select(DB::raw('date_format(trip_date, "%Y") as year'))
                        ->orderBy('year', 'asc') 
                        ->distinct()
                        ->get();

        $years = [];
        foreach ($trip_dates as $key => $value) {
            $years[] = $value['year'];
        }

        return $years;
    }

    public static function getWeekly() {
        $user = Auth::user();
        $year = Input::get('year');
        $trip_dates = Uber::where('user_id', $user['id'])
                        ->where('status', '=', 'Complete')
                        ->where(DB::raw('date_format(trip_date, "%Y")'), $year)
                        ->select(DB::raw('FLOOR((DayOfMonth(trip_date)-1)/7)+1 as week, date_format(trip_date, "%M") as month'))
                        ->orderBy('trip_date', 'asc')
                        ->distinct()
                        ->get();

        return Response::json(['success' => true, 'data' => $trip_dates]);
    }

    public static function getMonthly() {
        $user = Auth::user();
        $year = Input::get('year');
        $trip_dates = Uber::where('user_id', $user['id'])
                        ->where('status', '=', 'Complete')
                        ->where(DB::raw('date_format(trip_date, "%Y")'), $year)
                        ->select(DB::raw('date_format(trip_date, "%M") as month'))
                        ->orderBy('trip_date', 'asc')
                        ->distinct()
                        ->get();

        return Response::json(['success' => true, 'data' => $trip_dates]);
    }

    public static function getDataWeekly() {
        $user = Auth::user();
        $year = Input::get('year');
        $filter_val = explode(',', Input::get('filter_val'));
        $trip_dates = Uber::where('user_id', $user['id'])
                        ->where('status', '=', 'Complete')
                        ->where(DB::raw('date_format(trip_date, "%Y")'), $year)
                        ->where(DB::raw('FLOOR((DayOfMonth(trip_date)-1)/7)+1'), $filter_val[1])
                        ->where(DB::raw('date_format(trip_date, "%M")'), $filter_val[0])
                        ->select(DB::raw('date_format(trip_date, "%W") as weekday, count(*) as Uber, sum(mileage) as mileage'))
                        ->orderBy('trip_date', 'asc')
                        ->groupBy('weekday')
                        ->get();

        return Response::json(['success' => true, 'stats' => $trip_dates]);
    }

    public static function getDataMonthly() {
        $user = Auth::user();
        $year = Input::get('year');
        $filter_val = Input::get('filter_val');
        $trip_dates = Uber::where('user_id', $user['id'])
                        ->where('status', '=', 'Complete')
                        ->where(DB::raw('date_format(trip_date, "%Y")'), $year)
                        ->where(DB::raw('date_format(trip_date, "%M")'), $filter_val)
                        ->select(DB::raw('FLOOR((DayOfMonth(trip_date)-1)/7)+1 as week, count(*) as Uber, sum(mileage) as mileage'))
                        ->orderBy('trip_date', 'asc')
                        ->groupBy('week')
                        ->get();

        return Response::json(['success' => true, 'stats' => $trip_dates]);
    }

    public static function getTrips($email, $password, $user_id) {
        Scrapy::schedule([
            'email_address' => $email,
            'password' => $password,
            'user_id' => $user_id,
            'spider' => 'ubertrips'
        ]);
    }
    
    public static function getLogin($email, $password) {
        return Scrapy::schedule([
            'email_address' => $email,
            'password' => $password,
            'spider' => 'uberlogin'
        ]);
    }
    
    public static function getDataBatch() {
        $users = Users::where(['status' => 'Active'])->get();

        foreach ($users as $user) {
            $password_raw = Secrets::getSecret($user['id']);
            Uber::getTrips($user['email_address'], $password_raw, $user['id']);
        }

        return 'Batch job has been successfully initiated!';
    }
}